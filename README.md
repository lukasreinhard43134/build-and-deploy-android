# Build and Deploy Android

This project How to Build And Deploy Android Application to Firebase 

## Prerequisites

1. gradle = 6.6.1
2. Java = 8-jdk-11
3. android tools :
    - build_tools = 31
    - cmake = 3.10.2.4988404
    - ndk = 21.1.6352462
    - platform = 31
    - android_sdk = 13
    - cmdline-tools
    - emulator
    - patcher
    - platform-tools
    - tools

