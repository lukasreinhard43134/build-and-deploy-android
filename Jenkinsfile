pipeline {
    agent {
        node {
            label any
        }
    }
    environment{
        GIT_BRANCH = ${GIT_BRANCH}
        NOTES = ${NOTES}
        SPRINT_NUM = ${SPRINT_NUM}
        BUILD_FOR ${BUILD_FOR}
    }

    stages {
        stage("Initialization"){
            steps{
                echo "====++++executing Initialization++++===="
                cleanWs()
            }
            post{
                always{
                    echo "====++++always++++===="
                }
                success{
                    echo "====++++Initialization executed successfully++++===="
                }
                failure{
                    echo "====++++Initialization execution failed++++===="
                }
        
            }
        }
        stage("Clone Repository"){
            steps{
                echo "====++++executing Clone Repository ++++===="
                script{
                    git credentialsId: 'jenkinsuser',
                    url: "${GIT_HOSTNAME}/${GIT_GROUP_NAME}/${GIT_PROJECT_NAME}.git",
                    branch: "${GIT_BRANCH}"
                }
            }
            post{
                always{
                    echo "====++++always++++===="
                }
                success{
                    echo "====++++Clone Repository executed successfully++++===="
                }
                failure{
                    echo "====++++Clone Repository execution failed++++===="
                }
            }
        }
        stage("Configure Build Version"){
            steps{
                echo "====++++executing Configure Build Version in build.gradle ++++===="
                script{
                    sh '''
                    PACKAGE_VERSION=$(cat app/build.gradle | grep -e "def version\ *=\ *" | awk -F'["|"]' '{print $2}')
                    BUILD_DISPLAY_NUM="${PACKAGE_VERSION}-${BUILD_NUMBER}"
                    BUILD_VERSION_CODE=$(echo "${BUILD_DISPLAY_NUM}" | sed 's/[^0-9]*//g')

                    #handle versioncode digits
                    PACKAGE_VERSION_V2=$(grep "versionCode" app/build.gradle | awk '{print $2}')
                    BUILD_NUM=$((${BUILD_ID}))
                    BUILD_VERSION_CODE_V2=$((${PACKAGE_VERSION_V2} + ${BUILD_NUM}))

                    PACKAGE="mytelkomsel-android-native-$BUILD_DISPLAY_NUM.tgz"

                    echo PACKAGE_VERSION="${PACKAGE_VERSION}" > env.properties
                    echo BUILD_DISPLAY_NUM="${BUILD_DISPLAY_NUM}">> env.properties
                    echo BUILD_VERSION_CODE="${BUILD_VERSION_CODE}" >> env.properties
                    echo PACKAGE="${PACKAGE}" >> env.properties

                    echo ":::Starting get repository:::"
                    echo "Branch: ${GIT_BRANCH}"
                    echo "Build Number: ${BUILD_DISPLAY_NUM}"

                    #checking if buildVersion is exist or not
                    if grep -q "buildVersion" app/build.gradle; 
                    then
                        sed -i".orig" "s/buildVersion *= *version/buildVersion=\"${BUILD_DISPLAY_NUM}\"/g"  app/build.gradle
                        sed -i".orig" 's/versionName *version/versionName buildVersion/g' app/build.gradle
                    else
                    #if not then create one
                        sed -i".orig" '/version *= *"'${PACKAGE_VERSION}'"/a\'$'\n''def buildVersion="'${BUILD_DISPLAY_NUM}'"'$'\n' app/build.gradle
                        sed -i".orig" 's/versionName *version/versionName buildVersion/g' app/build.gradle
                    fi

                    sed -i".orig" "s/versionCode [0-9]*/versionCode ${BUILD_VERSION_CODE_V2}/g" app/build.gradle

                    #zip the project
                    tar -czf $PACKAGE *
                    '''
                }
            }
            post{
                always{
                    echo "====++++always++++===="
                }
                success{
                    echo "====++++Configure Build Version executed successfully++++===="
                }
                failure{
                    echo "====++++Configure Build Version execution failed++++===="
                }
            }
        }
        stage("Build Code Android"){
            steps{
                echo "====++++executing Build Code Android ++++===="
                script{
                    sh '''
                        source ~/.bash_profile
                        TARFILE=mytelkomsel-android-native-$ARTIFACT_ID.tgz
                        KEYSTORE=/Users/phincon/.jenkins/jobs/Android_Build_PreReq/PlayStoreMyTelkomsel2.keystore
                        STOREPASS=J4k4rtA1
                        ALIAS=wtg
                        KEYTOOL=/usr/local/opt/openjdk@8/bin/keytool
                        JARSIGNER=jarsigner
                        APKSIGNER=/Applications/android/build-tools/31.0.0/apksigner
                        ZIPALIGN=/Applications/android/build-tools/31.0.0/zipalign
                        APK_FILE=/Users/phincon/.jenkins/workspace/Android_Native_Prod/app/build/outputs/apk/production/release/app-production-release-unsigned.apk
                        APK_AAB=/Users/phincon/.jenkins/workspace/Android_Native_Prod/app/build/outputs/bundle/productionRelease/app-production-release.aab
                        ZIPALIGNED_FILENAME_AAB=MT-Prod-Release-$ARTIFACT_ID.aab
                        ZIPALIGNED_FILENAME=MT-Prod-Release-$ARTIFACT_ID.apk

                        ls -lh

                        #running gradle
                        gradle ${BUILD_FOR} || gradle --offline ${BUILD_FOR}

                        verify_tools() {
                            OUTPUT=$( ${JARSIGNER} -help 2>&1 | grep "verify" )
                            if [ $? -ne 0 ]; then
                                echo "Could not run jarsigner tool, please check settings"
                                exit 1
                            fi
                            
                            # Check 'zipalign' tool
                            echo ${ZIPALIGN}
                            OUTPUT=$( ${ZIPALIGN} 2>&1 | grep -i "Zip alignment" )
                            echo $OUTPUT
                            if [ $? -ne 0 ]; then
                                echo "Could not run zipalign tool, please check settings"
                                exit 1
                            fi

                            OUTPUT=$( ${KEYTOOL} -help 2>&1 | grep "keypasswd" )
                            echo $OUTPUT
                            if [ $? -ne 0 ]; then
                                echo "Could not run keytool tool, please check settings"
                                exit 1
                            fi
                        }
                        verify_settings() {
                            if [ -z "${KEYSTORE}" -o -z "${STOREPASS}" -o -z "{$ALIAS}" ]; then
                                usage
                                echo "Please update KEYSTORE, STOREPASS and ALIAS with your jar signing credentials"
                                exit 1
                            fi

                            # verify KEYSTORE, STOREPASS and ALIAS at once
                            OUTPUT=$( ${KEYTOOL} -list -keystore "${KEYSTORE}" -storepass "${STOREPASS}" -alias "${ALIAS}" 2>&1 )
                            if [ $? -ne 0 ]; then
                                usage
                                echo "Please check keystore credentials; keytool failed to verify storepass and alias"
                                exit 1
                            fi
                        }
                    '''
                }
            }
            post{
                always{
                    echo "====++++always++++===="
                }
                success{
                    echo "====++++Build Code Android executed successfully++++===="
                }
                failure{
                    echo "====++++Build Code Android execution failed++++===="
                }
            }
        }
        stage("Deploy Application Android"){
            steps{
                echo "====++++executing Deploy Application Android ++++===="
                script{
                    sh '''
                        if [ ${BUILD_FOR} == "assembleProductionRelease" ]
                        then
                            echo "Signing APK file.. "
                            
                            ${ZIPALIGN} -f -v 4 ${APK_FILE} ${ZIPALIGNED_FILENAME}
                            ${APKSIGNER} sign --ks ${KEYSTORE} --ks-pass pass:${STOREPASS} --verbose --ks-key-alias ${ALIAS} --min-sdk-version 24 --max-sdk-version 31 ${ZIPALIGNED_FILENAME}
                            ${APKSIGNER} verify --verbose ${ZIPALIGNED_FILENAME}
                            
                            echo "OK Release APK!"
                        elif [ ${BUILD_FOR} == "bundleProductionRelease" ]
                        then
                            echo "Signing AAB file.. "
                            ${ZIPALIGN} -f -v 4 ${APK_AAB} ${ZIPALIGNED_FILENAME_AAB}
                            ${APKSIGNER} sign --ks ${KEYSTORE} --ks-pass pass:${STOREPASS} --verbose --ks-key-alias ${ALIAS} --min-sdk-version 24 --max-sdk-version 30 ${ZIPALIGNED_FILENAME_AAB}
                            
                            echo "OK Release AAB!"
                        else 
                            APK_FILE=app/build/outputs/apk/production/debug/app-production-debug.apk
                            ZIPALIGNED_FILENAME=MT-Prod-Debug-$ARTIFACT_ID.apk

                            echo "Rename Application"
                            cp ${APK_FILE} ${ZIPALIGNED_FILENAME}

                        if [ $BUILD_FOR = "assembleProductionDebug" ]; then
                            echo "uploading to firebase..."
                            /usr/local/bin/firebase appdistribution:distribute ${ZIPALIGNED_FILENAME}  --app 1:655740818190:android:7eca7aaf9347a457  --release-notes "MyTelkomsel Android version ${ARTIFACT_ID}n\nNote: ${NOTES}\nSprint : ${SPRINT_NUM}\nMode: Debug" --groups "tdw-android-qa-team"
                        elif [ $BUILD_FOR = "bundleProductionRelease" ]; then
                            echo "uploading to firebase..."
                            /usr/local/bin/firebase appdistribution:distribute ${ZIPALIGNED_FILENAME_AAB}  --app 1:655740818190:android:7eca7aaf9347a457  --release-notes "MyTelkomsel Android version ${ARTIFACT_IDn}\nNote: ${NOTES}\nSprint : ${SPRINT_NUM}\nMode: Release" --groups "fut-team"
                        elif [ $BUILD_FOR = "bundleProductionRelease-assembleProductionRelease" ]; then
                            echo "uploading AAB to firebase..."
                            /usr/local/bin/firebase appdistribution:distribute ${ZIPALIGNED_FILENAME_AAB}  --app 1:655740818190:android:7eca7aaf9347a457  --release-notes "MyTelkomsel Android version ${ARTIFACT_IDn}\nNote: ${NOTES}\nSprint : ${SPRINT_NUM}\nMode: Release" --groups "fut-team"
                            echo "uploading APK to firebase..."
                            /usr/local/bin/firebase appdistribution:distribute ${ZIPALIGNED_FILENAME}  --app 1:655740818190:android:7eca7aaf9347a457  --release-notes "MyTelkomsel Android version ${ARTIFACT_IDn}\nNote: ${NOTES}\nSprint : ${SPRINT_NUM}\nMode: Release" --groups "fut-team"
                        else
                            echo "uploading to firebase..."
                            /usr/local/bin/firebase appdistribution:distribute ${ZIPALIGNED_FILENAME}  --app 1:655740818190:android:7eca7aaf9347a457  --release-notes "MyTelkomsel Android version ${ARTIFACT_IDn}\nNote: ${NOTES}\nSprint : ${SPRINT_NUM}\nMode: Release" --groups "fut-team"
                        fi
                    '''
                }
            }
            post{
                always{
                    echo "====++++always++++===="
                }
                success{
                    echo "====++++Deploy Application Android executed successfully++++===="
                }
                failure{
                    echo "====++++Deploy Application Android execution failed++++===="
                }
            }
        }
    }
}